# Help

Need help with the ircd or services? Check these documentation pages before 
consulting help from our `#help` channel.

Click [here](cmode.html) to see the currently available channel modes on 
ShadowNET.

