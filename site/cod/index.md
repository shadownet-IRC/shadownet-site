Cod
===



Extended IRC services in Python

Installation directions are in `doc/INSTALL.markdown`. Directions for setting
up Atheme integration are in `doc/funserv.markdown`. If you have any questions,
please ask in `#cod` on `irc.yolo-swag.com`.

### Features:

 - Asynchronous socket handling
   (and an easy way for modules to add socket handlers)
 - Dynamic channel joins
 - Forking to background
 - Logging to snoop channel
 - Modular loading and unloading
 - No specific libc dependency (tested on glibc, uclibc and musl)
 - Rehashing config file
 - Separation of user and oper commands
 - SQLite database
 - TS6 link protocol support
 - Virtual environment support

### Goals:

 - DNS record editing (Tortoise Labs)
 - Feature-completeness with skybot
 - MPD playlist manipulation
 - User statistics collection
   - Follow anonyminity standards of the Tor Project
 - Web GUI for administration

### Stretch Goals:

 - Automated provisioning and linking of temporary overflow IRC daemons
 - Spam filtering (opt-in only)

The official channel for Cod is `#cod` on `irc.yolo-swag.com`. Come take
a visit and say hi!

