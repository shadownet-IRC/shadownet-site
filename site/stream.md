ShadowNET offers a mostly 24/7 music stream. To listen to it, click 
[here](http://sparkle.yolo-swag.com:8001/stream.ogg).

This stream is run on MPD's built-in HTTPD streamer, and can be added to your 
favorite music player with ease.

