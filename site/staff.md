# Staff

This list is not exhaustive.

## Administrators

### Xe

The founder of ShadowNET, Xe has guided the creation of elemental-ircd and 
worked hard to make Cod and Atheme work symbiotically with it. Their favorite 
color is blue and you can find them on github [here](http://github.com/lyska). 
Might sometimes be online as `shadowh511`, `lyska` or `Xena`.

### Caerdwyn

The founder of DashNet, an IRC network that merged into ShadowNET, a capable 
Linux system administrator who is still learning (as we all are), but has 
proven himself to be ready for the task. Bug him if there's something wrong 
with the website, the server is down, etc.

## Operators

### Lore

See one of the DashNet ZNC accounts connected somewhere, or does our Minecraft 
server intrigue you? Contact him if you want in.

