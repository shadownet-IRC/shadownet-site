# Services

## [Atheme IRC Services](http://atheme.net)

Atheme is a feature-packed, extremely customizable IRC services daemon that is 
secure, stable and scalable. Compatible with most IRCv3 compliant daemons and 
some that aren't, it is one of the most widely known and used services packages 
today. More information available on their website. Just click the title!

## [ZNC Network Bouncer](http://znc.in)

ZNC, an IRC Network Bouncer, keeps your connection online, even when your 
client is closed. It keeps your channels, PMs, modes and all saved for you 
until you login again, making it seem as if you never went offline.

We currently offer several bouncers, please poke either Xe or Lore on IRC to 
get more information.

## [Cod](http://github.com/lyska/cod)

Our home-grown extended services package. It handles a lot of things that an 
IRC bot normally would, but has the advantage of being a full blown server link 
and is stupidly fast. See more [here](cod).

## Minecraft

Join the server that started it all at `mc.yolo-swag.com`. Server chat is 
relayed to `#6fmc`. 

The `rdash.in` server is online, following a few upgrades.
Want to join? Contact Caerdwyn or Lore to be whitelisted!

## Janus

Want to link your channel back to its previous home on another network? Contact 
one of the staff in `#help` and let us know, we'll do what we can to link it 
back together.

