# ShadowNET Administrative Policy

ShadowNET operators and administrators will not get involved in channel matters 
unless they have flags in that channel. ShadowNET staff will not care about 
what goes on in channels unless they need to or are talking in them. ShadowNET 
staff is not any different from normal users except for a flag and a lot of 
extra work.

K-lines are reserved only for those who truly earn them.

 - If /ignore solves the problem rather than a kick, /ignore
 - Kick if a ban is unneeded
 - Ban if a /kill is unwarranted for
 - Kill rather than kline if that solves the problem
 - Kline when a server ban is really needed.

Please do not ask to be ShadowNET staff. If the current staff body believes in 
good faith that a user is capable of becoming ShadowNET staff, they will be 
picked for the role.

At this time ShadowNET is not accepting external links, but does have an 
instance of the Janus channel interlinker running and is happy to try and 
connect it to your channel on another network to ease migration.

