## Servers

Right now ShadowNET has the following client servers available:

 - `sparkle.yolo-swag.com` - "Twilight Sparkle"
 - `volantis.yolo-swag.com` - "Tau Volantis"
 - `twink.yolo-swag.com` - "Cute little Twink~"
 - `cloudchaser.yolo-swag.com` - "Wonderbolt academy dropout"
 - `rdash.in` - "Rainbow Dash"

All of our servers are listening on port `6667` for plaintext connections.
For SSL connections, use port `6697`.

Attention bot authors: Please note that we do use TCP defer-accept so please 
send data before you wait to get any.

